<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Services</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'/>
    <!-- use the font -->
    <style>
        .body{
            font-size: 10.0pt;
            line-height: 115%;
            font-family: "Arial",sans-serif;
            color: #595959;
        }
        .header{
            font-size: 22.0pt;
            line-height: 150%;
            font-family: "Arial",sans-serif;
            color: #DC0032;
        }
    </style>
</head>
<body style="background-color:#d3d3d3; font-size: 10.0pt;
                                              line-height: 115%;
                                              font-family: "Arial",sans-serif;
color: #595959;" class="body" >
<div style="background-color:#fff;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="70%" style="border-collapse: collapse; background-color:#fff">
        <tr>
            <td>
                <img src ="https://s3.us-east-2.amazonaws.com/www.chefsonapp.co.za/media/fwchefsonapp/output-onlinepngtools+(1).png" width="45%" style="margin-left: 25%">
            </td>
        </tr>
        <tr>
            <td style="padding: 40px 30px 40px 30px;">
                <p style="font-size:14.0pt; line-height:100%; font-family:"Arial",Helvetica, sans-serif; color:#595959">Hi,${firstName}</br></p>
                <p style="font-size:14.0pt; line-height:100%; font-family:"Arial",Helvetica,sans-serif; color:#595959">${content}</p></br>
                <p style="font-size:14.0pt; line-height:100%; font-family:"Arial",Helvetica,sans-serif; color:#595959">${setpasswordLink}</p></br>
                <br>
            </td>
        </tr>
        <tr>
            <td>

            </td>
        </tr>
        <tr >
            <td style="padding-left: 30px">
                <p pan style="font-size:17.0pt; line-height:100%; font-family:"Arial",Helvetica,sans-serif; color:#595959">Kind Regards</p>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px"><span style="font-size:17.0pt; line-height:100%; font-family:"Arial",Helvetica,sans-serif; color:#595959">Chefs On App</span> </td>
        </tr>
    </table>
</div>
</body>
</html>

