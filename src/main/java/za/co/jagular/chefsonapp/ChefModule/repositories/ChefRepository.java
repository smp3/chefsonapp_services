package za.co.jagular.chefsonapp.ChefModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ChefModule.entities.Chef;

public interface ChefRepository extends JpaRepository<Chef, String> {
    Boolean existsByEmail(String email);
    Boolean existsByIdToken(String idToken);
    Chef findByIdToken(String idToken);
    Chef findByEmail(String email);

}
