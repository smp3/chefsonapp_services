package za.co.jagular.chefsonapp.ChefModule.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.ChefModule.dto.QuestionnaireDTO;
import za.co.jagular.chefsonapp.ChefModule.dto.QuestionnaireResponse;
import za.co.jagular.chefsonapp.ChefModule.entities.Question;
import za.co.jagular.chefsonapp.ChefModule.entities.Questionnaire;
import za.co.jagular.chefsonapp.ChefModule.repositories.QuestionRepositoryImpl;
import za.co.jagular.chefsonapp.ChefModule.repositories.QuestionnaireRepositoryImpl;


import java.util.List;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepositoryImpl questionDAO;
    @Autowired
    private QuestionnaireRepositoryImpl questionnaireDAO;

    Logger logger = LoggerFactory.getLogger(QuestionService.class);

    public ResponseEntity<?> getAllQuestions() {
        logger.info("Finding all questions");
        ResponseEntity responseEntity = null;
        try {
            List<Question> allQuestions = questionDAO.findAllQuestions();
            if (allQuestions != null) {
                responseEntity = ResponseEntity.status(HttpStatus.OK).body(allQuestions);
            } else {
                responseEntity = ResponseEntity.status(HttpStatus.NO_CONTENT).body("Something went wrong whilst trying to obtain questions.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Something went wrong", e.getMessage(), e.getCause());
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
        return responseEntity;
    }

    public ResponseEntity<?> markQuestions(QuestionnaireDTO questionnaireDTO) {
        logger.info("Question marking service");
        try {
            int score = 0;
            List<Question> allQuestions = questionDAO.findAllQuestions();
            if (allQuestions!=null) {
                Questionnaire questionnaireProfile = new Questionnaire();
                questionnaireProfile.setEmail(questionnaireDTO.getEmail());
                questionnaireProfile.setFullname(questionnaireDTO.getFullname());
                //answer calculation
                for (QuestionnaireResponse response : questionnaireDTO.getResponses()) {
                    int i = Integer.parseInt(response.getQuestionNumber());
                    if (allQuestions.get(i-1).getCorrectAnswerCode().equalsIgnoreCase(response.getAnswerCode())) {
                        score++;
                    }
                }
                if (score<allQuestions.size())
                    questionnaireProfile.setResult("Failed");
                else
                    questionnaireProfile.setResult("Passed");
                questionnaireProfile.setScore(score);
                if (questionnaireDAO.saveProfile(questionnaireProfile)){
                    return ResponseEntity.status(HttpStatus.OK).body(questionnaireProfile);
                }
                else {
                    return  ResponseEntity.status(HttpStatus.NO_CONTENT).body("Unable to save \n"+questionnaireProfile);
                }
            }else
            {
                throw new RuntimeException("No questions found");
            }

        }catch (RuntimeException re){
            re.getCause();
            re.printStackTrace();
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(re.getMessage());

        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");

        }

    }
}
