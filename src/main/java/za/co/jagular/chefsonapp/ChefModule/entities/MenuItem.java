package za.co.jagular.chefsonapp.ChefModule.entities;



import za.co.jagular.chefsonapp.ChefModule.Utilities.AuditModel;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "MenuItems")
public class MenuItem extends AuditModel {
    @Id
    private String id;
    private String itemName;
    @Column(columnDefinition = "TEXT")
    private String picture;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String status;
    private String itemType;


    @ManyToOne
    private Menu menu;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public MenuItem() {
        this.id = UUID.randomUUID().toString();
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
