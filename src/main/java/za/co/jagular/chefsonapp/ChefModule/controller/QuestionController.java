package za.co.jagular.chefsonapp.ChefModule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.jagular.chefsonapp.ChefModule.dto.QuestionnaireDTO;
import za.co.jagular.chefsonapp.ChefModule.services.QuestionService;


import javax.validation.Valid;

@RestController
@CrossOrigin
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @GetMapping("/get/questions")
    public ResponseEntity<?> getQuestions(){
        return questionService.getAllQuestions();
    }
    @PostMapping("/submit/questionnaire")
    public ResponseEntity<?> submitQuestinnaire(@Valid @RequestBody QuestionnaireDTO questionnaireDTO){
        return questionService.markQuestions(questionnaireDTO);
    }
}
