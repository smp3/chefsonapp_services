package za.co.jagular.chefsonapp.ChefModule.dto;

import java.util.List;

public class QuestionnaireDTO {
    private String id;
    private String fullname;
    private String email;
    private List<QuestionnaireResponse> responses;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<QuestionnaireResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<QuestionnaireResponse> responses) {
        this.responses = responses;
    }
}
