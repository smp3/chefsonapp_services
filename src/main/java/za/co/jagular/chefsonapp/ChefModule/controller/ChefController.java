package za.co.jagular.chefsonapp.ChefModule.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import za.co.jagular.chefsonapp.ChefModule.dto.ChefAuthDTO;
import za.co.jagular.chefsonapp.ChefModule.dto.ChefDTO;
import za.co.jagular.chefsonapp.ChefModule.services.ChefService;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@CrossOrigin
public class  ChefController {
    @Autowired
    private ChefService chefService;
    Logger logger = LoggerFactory.getLogger(ChefController.class);

    @PostMapping("/save/chef")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> storeChef(@Valid @RequestBody ChefDTO chef, @RequestHeader HashMap<String, String> headers){
        return chefService.saveAchef(chef,headers);
    }

    @PostMapping("/set/chef/password")
    public ResponseEntity<?> chefRegistration(@Valid @RequestBody ChefAuthDTO chefAuthDTO){
        return  chefService.enrollChefAsUser(chefAuthDTO);

    }
}
