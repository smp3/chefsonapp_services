package za.co.jagular.chefsonapp.ChefModule.dto;

import java.util.List;

public class MenuDTO {
    private String category;
    private String name;
    private List<MenuItemDTO> menuItems;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MenuItemDTO> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemDTO> menuItems) {
        this.menuItems = menuItems;
    }
}
