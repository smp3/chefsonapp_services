package za.co.jagular.chefsonapp.ChefModule.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import za.co.jagular.chefsonapp.ChefModule.dto.MenuDTO;
import za.co.jagular.chefsonapp.ChefModule.entities.Menu;
import za.co.jagular.chefsonapp.ChefModule.repositories.MenuRepositoryImpl;


@RestController
@CrossOrigin
public class MenuService {
    @Autowired
    private MenuRepositoryImpl menuDAO;
    Logger logger = LoggerFactory.getLogger(MenuService.class);

    public ResponseEntity<?> saveMenu(MenuDTO menuDTO) {
        logger.info("Saving menu");
        Menu menuEntity = new Menu();
        ResponseEntity responseEntity = null;
        try {
            menuEntity.setCategory(menuDTO.getCategory());
            menuEntity.setName(menuDTO.getName());
            if (menuDAO.saveMenu(menuEntity)) {
                responseEntity = ResponseEntity.status(HttpStatus.OK).body(menuEntity);
                logger.info("Menu Saved successfully");
            }
                else {
                responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unable to save menu");
                logger.info("Unable to save menu");
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Something went wrong");
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
        return responseEntity;
    }
}
