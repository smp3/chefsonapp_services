package za.co.jagular.chefsonapp.ChefModule.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.ChefModule.dto.ChefAuthDTO;
import za.co.jagular.chefsonapp.ChefModule.dto.ChefDTO;
import za.co.jagular.chefsonapp.ChefModule.entities.Chef;
import za.co.jagular.chefsonapp.ChefModule.repositories.ChefRepository;
import za.co.jagular.chefsonapp.ChefModule.repositories.ChefRepositoryImpl;
import za.co.jagular.chefsonapp.jwt.dto.responses.ErrorDTO;
import za.co.jagular.chefsonapp.jwt.entities.authentication.Role;
import za.co.jagular.chefsonapp.jwt.entities.authentication.RoleName;
import za.co.jagular.chefsonapp.jwt.entities.authentication.User;
import za.co.jagular.chefsonapp.jwt.repository.RoleRepository;
import za.co.jagular.chefsonapp.jwt.repository.UserRepository;
import za.co.jagular.chefsonapp.jwt.security.jwt.JwtProvider;
import za.co.jagular.chefsonapp.mailer.services.MailDispatcher;
import za.co.jagular.chefsonapp.mailer.services.MailerService;
import za.co.jagular.chefsonapp.profile.Profile;
import za.co.jagular.chefsonapp.profile.ProfileRepository;
import za.co.jagular.chefsonapp.responses.ResponseDTO;

import java.util.*;

@Service
public class ChefService {

    @Autowired
    private ChefRepositoryImpl chefDAO;
    @Autowired
    private ChefRepository  chefRepository;
    @Autowired
    private MailDispatcher mailDispatcher;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    RoleRepository roleRepository;

    Logger logger = LoggerFactory.getLogger(ChefService.class);
    public ResponseEntity<?> saveAchef(ChefDTO chef, HashMap<String, String> headers){
        logger.info("Saving Chef Service");
        Chef chefEntity = new Chef();
        ModelMapper mapper = new ModelMapper();

        ResponseEntity responseEntity = null;
        try {
            //Find existing chef first
            if(chefRepository.existsByEmail(chef.getEmail())){
                ErrorDTO errorDTO = new ErrorDTO();
                errorDTO.setMessage("Chef has already been registered on the platform");
                errorDTO.setErrorCode("400");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO);
            }else {

                chefEntity = mapper.map(chef, Chef.class);
                chefEntity.setStatus("Pending");
                chefEntity.setIdToken(UUID.randomUUID().toString());
                if (chefDAO.saveChef(chefEntity)) {
                    responseEntity = ResponseEntity.status(HttpStatus.OK).body(chefEntity);
                    logger.info("Chef saved successfully", chef);
                    if(mailDispatcher.dispatchMail(chef)){
                        logger.info("Registration mail sent out to chef", chef);
                    }else
                    {
                        logger.error("Unable to send mail");
                    }
                } else {
                    responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unable to save chef.");
                    logger.info("Unable to save chef", chef);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.info("SERVICE LAYER: Exception thrown while trying to save chef");
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
        return  responseEntity;
    }
    public ResponseEntity<?> enrollChefAsUser(ChefAuthDTO chefAuthDTO){
        logger.info("enrolling chef");
        try {
            logger.info("finding chef by id_token");
            Chef chef = chefDAO.getChefByToken(chefAuthDTO.getToken());
            if(chef != null){
                logger.info("Creating entry for Chef");
                Optional<Role> userRole = roleRepository.findByName(RoleName.ROLE_CHEF);
                Set<Role> roles = new HashSet<>();
                roles.add(userRole.get());

                if(userRepository.existsByEmail(chef.getEmail())){
                    ErrorDTO errorDTO = new ErrorDTO();
                    errorDTO.setErrorCode("400");
                    errorDTO.setMessage("Chef Already enrolled");
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(errorDTO);
                }else {
                    User chefUser = new User();
                    chefUser.setSurname(chef.getSurname());
                    chefUser.setPassword(encoder.encode(chefAuthDTO.getPassword()));
                    chefUser.setUsername(chef.getEmail());
                    chefUser.setName(chef.getName());
                    chefUser.setApplication("Chefs On App");
                    chefUser.setRoles(roles);
                    chefUser.setEmail(chef.getEmail());
                    logger.info("Saving Chef User");
                    userRepository.save(chefUser);
                    logger.info("Chef User Saved");

                    ResponseDTO responseDTO = new ResponseDTO();
                    responseDTO.setMessage("Chef registered");
                    responseDTO.setCode("200");
                    logger.info("Chef saved succesfully");
                    return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
                }
            }else {
                ErrorDTO errorDTO = new ErrorDTO();
                errorDTO.setErrorCode("401");
                errorDTO.setMessage("Not Authorized On The Platform");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorDTO);
            }
        }catch (Exception e){
            e.printStackTrace();
            ErrorDTO errorDTO = new ErrorDTO();
            errorDTO.setErrorCode("500");
            errorDTO.setMessage("Something went wrong");
            logger.error("Something went wrong");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDTO);
        }

    }
}
