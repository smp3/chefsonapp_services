package za.co.jagular.chefsonapp.ChefModule.dto;

public class QuestionnaireResponse {
    private String questionNumber;
    private String answerCode;

    public String getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(String answerCode) {
        this.answerCode = answerCode;
    }
}
