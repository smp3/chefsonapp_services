package za.co.jagular.chefsonapp.ChefModule.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.ChefModule.entities.Menu;


import java.util.List;

@Component
public class MenuRepositoryImpl {
    @Autowired
    private MenuRepository menuDAO;
    Logger logger = LoggerFactory.getLogger(MenuRepositoryImpl.class);

    public boolean saveMenu(Menu menu){
        logger.info("Saving menu");
        try {
            menuDAO.save(menu);
            logger.info("menu saved");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            logger.error("An exception occured while trying to save menu");
            return false;
        }
    }

    public List<Menu> getMenu(){
        logger.info("Get menu");
        try {
            return menuDAO.findAll();
        }catch (Exception e){
            e.printStackTrace();
            logger.error("An exception occured while trying get menu");
            return null;
        }
    }
}
