package za.co.jagular.chefsonapp.ChefModule.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Questions")
public class Question  {

    @Id
    private String id;
    private int questionNumber;
    @Column(columnDefinition = "TEXT")
    private String question;
    @OneToMany(mappedBy = "question")
    private List<Answer> answers = new ArrayList<>();
    @JsonIgnore
    private String correctAnswerCode;


    public String getId() {
        return id;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getCorrectAnswerCode() {
        return correctAnswerCode;
    }

    public void setCorrectAnswerCode(String correctAnswerCode) {
        this.correctAnswerCode = correctAnswerCode;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

}
