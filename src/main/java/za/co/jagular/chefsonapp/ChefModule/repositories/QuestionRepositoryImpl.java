package za.co.jagular.chefsonapp.ChefModule.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.ChefModule.entities.Question;


import java.util.List;

@Component
public class QuestionRepositoryImpl {
    @Autowired
    private QuestionRepository questionRepository;

    public List<Question> findAllQuestions(){
        try {
            return  questionRepository.findAll();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
