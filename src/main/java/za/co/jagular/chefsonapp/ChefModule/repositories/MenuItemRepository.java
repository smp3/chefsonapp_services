package za.co.jagular.chefsonapp.ChefModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ChefModule.entities.MenuItem;

public interface MenuItemRepository extends JpaRepository<MenuItem, String> {
}
