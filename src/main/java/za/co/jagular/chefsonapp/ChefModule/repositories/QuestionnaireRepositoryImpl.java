package za.co.jagular.chefsonapp.ChefModule.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.ChefModule.entities.Questionnaire;

@Component
public class QuestionnaireRepositoryImpl {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    public boolean saveProfile(Questionnaire questionnaireProfile){
        try {
            questionnaireRepository.save(questionnaireProfile);
            System.out.println("Saved Q's");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;

        }
    }
}
