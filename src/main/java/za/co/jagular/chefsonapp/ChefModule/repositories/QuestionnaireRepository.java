package za.co.jagular.chefsonapp.ChefModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ChefModule.entities.Questionnaire;

public interface QuestionnaireRepository extends JpaRepository<Questionnaire, String> {
}
