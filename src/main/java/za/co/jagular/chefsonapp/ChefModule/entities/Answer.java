package za.co.jagular.chefsonapp.ChefModule.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "Answers")
public class Answer {
    @Id
    private String id;
    private String code;
    @Column(columnDefinition = "TEXT")
    private String answerValue;
    @JsonIgnore
    @ManyToOne
    private Question question;

    public String getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }
}
