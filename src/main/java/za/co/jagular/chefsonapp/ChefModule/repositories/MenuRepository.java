package za.co.jagular.chefsonapp.ChefModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ChefModule.entities.Menu;

public interface MenuRepository extends JpaRepository<Menu, String> {
}
