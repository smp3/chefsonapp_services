package za.co.jagular.chefsonapp.ChefModule.repositories;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.ChefModule.entities.Chef;

import java.util.List;

@Component
public class ChefRepositoryImpl {
    @Autowired
    private ChefRepository chefRepository;
    Logger logger = LoggerFactory.getLogger(ChefRepositoryImpl.class);

    public boolean saveChef(Chef chef){
        logger.info("Data layer: Saving chef");
        try {
            chefRepository.save(chef);
            logger.info("Chef saved");
            return true;

        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return false;
        }
    }

    public List<Chef> findAllChefs(){
      logger.info("Data layer: finding chefs");
        try {
            return chefRepository.findAll();
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return null;

        }
    }

    public Chef getChefByToken(String token){
        try {
            return chefRepository.findByIdToken(token);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public Chef getChefByEmail(String email){
        try {
            return chefRepository.findByIdToken(email);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }


}
