package za.co.jagular.chefsonapp.ChefModule.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ChefModule.entities.Question;

public interface QuestionRepository extends JpaRepository<Question, String> {
}
