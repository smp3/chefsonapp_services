package za.co.jagular.chefsonapp.mailer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.ChefModule.dto.ChefDTO;
import za.co.jagular.chefsonapp.mailer.config.ApplicaitonConfiguration;
import za.co.jagular.chefsonapp.mailer.models.Mail;


import java.util.HashMap;
import java.util.Map;

@Service
public class MailDispatcher {

    @Autowired
    EmailService emailService;
    @Autowired
    MailerService mailerService;

    public ResponseEntity<?> sendMail(){
        return null;
    }
    public boolean dispatchMail(ChefDTO chefDTO){
        Mail dispatchedMAil = new Mail();
        dispatchedMAil.setMailFrom("no-reply@jaularservices.co.za");
        dispatchedMAil.setMailTo(chefDTO.getEmail());
        dispatchedMAil.setMailSubject("JWS - Jagular Web Services");

        String  message = "You have been registered on Chefs on App. \n Please click the link below to set your password";
        String passwordResetLink = "https://www.google.com";
        Map< String, Object > model = new HashMap< String, Object >();
        model.put("firstName", chefDTO.getName());
        model.put("content", message);
        model.put("setpasswordLink", passwordResetLink);

        dispatchedMAil.setModel(model);

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicaitonConfiguration.class);
        if (emailService.sendEmail(dispatchedMAil)){
            context.close();
            return true;
        }else {
            return false;
        }

    }
}
