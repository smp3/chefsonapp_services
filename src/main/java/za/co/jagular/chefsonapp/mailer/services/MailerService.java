package za.co.jagular.chefsonapp.mailer.services;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import za.co.jagular.chefsonapp.mailer.models.Mail;

import javax.mail.internet.MimeMessage;

@Service
public class MailerService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    Configuration fmConfiguration;

    public boolean sendEmail(Mail mail) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            Template template = fmConfiguration.getTemplate("default.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mail.getModel());

            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText(html, true);

            mailSender.send(mimeMessageHelper.getMimeMessage());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



}
