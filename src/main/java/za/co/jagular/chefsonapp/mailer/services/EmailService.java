package za.co.jagular.chefsonapp.mailer.services;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import za.co.jagular.chefsonapp.mailer.models.Mail;


import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Configuration configuration;

    public boolean sendEmail(Mail mail){
        try{

            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            helper.setTo(mail.getMailTo());
            //helper.setText();
            helper.setSubject(mail.getMailSubject());
            helper.setFrom(mail.getMailFrom());

            Template template = configuration.getTemplate("default.ftl");
            String htmlPage = FreeMarkerTemplateUtils.processTemplateIntoString(template,mail.getModel());
            helper.setText(htmlPage,true);
            javaMailSender.send(mimeMessage);
return true;

        }catch (Exception ex){
            ex.printStackTrace();

            return false;

        }
    }
}
