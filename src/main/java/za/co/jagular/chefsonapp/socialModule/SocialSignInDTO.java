package za.co.jagular.chefsonapp.socialModule;

public class SocialSignInDTO {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
