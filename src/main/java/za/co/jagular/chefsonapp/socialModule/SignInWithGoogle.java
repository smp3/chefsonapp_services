package za.co.jagular.chefsonapp.socialModule;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.jwt.dto.authentication.JwtResponse;
import za.co.jagular.chefsonapp.jwt.dto.responses.ErrorDTO;
import za.co.jagular.chefsonapp.jwt.entities.authentication.Role;
import za.co.jagular.chefsonapp.jwt.entities.authentication.RoleName;
import za.co.jagular.chefsonapp.jwt.entities.authentication.User;
import za.co.jagular.chefsonapp.jwt.repository.RoleRepository;
import za.co.jagular.chefsonapp.jwt.repository.UserRepository;
import za.co.jagular.chefsonapp.jwt.security.jwt.JwtProvider;
import za.co.jagular.chefsonapp.otp.OTPRequestDTO;
import za.co.jagular.chefsonapp.profile.Profile;
import za.co.jagular.chefsonapp.profile.ProfileRepository;
import za.co.jagular.chefsonapp.responses.RestLogger;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static io.restassured.RestAssured.given;

@Service
public class SignInWithGoogle {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder encoder;


    private Logger logger = LoggerFactory.getLogger(SignInWithGoogle.class);

    private Response googleAuthentication(String token){
        logger.info("Calling google to verify token");
        RestAssured.baseURI = "https://oauth2.googleapis.com";
        Response response = null;
        try{
            response = given().contentType(ContentType.JSON)
                    .get("/tokeninfo?id_token="+token);
            System.out.println("Response :" + response.asString());
            System.out.println("Status Code :" + response.getStatusCode());


        }
        catch (Exception e){
            e.printStackTrace();
            logger.error("Call to google failed");
        }
        return response;
    }

    public ResponseEntity<?> continueWithGoogle(SocialSignInDTO socialSignInDTO){
        logger.info("Continue with google service");
        User socialUser = new User();
        Profile userProfile = new Profile();
        try{
            Response response =googleAuthentication(socialSignInDTO.getToken());

            if(response.getStatusCode()==200){
                //If all goes well with google, create default profile
                String email = response.jsonPath().getString("email");
               if (userRepository.existsByEmail(email)){
                   Optional<User> user = userRepository.findByUsername(email);
                   try {
                       Authentication authentication = authenticationManager.authenticate(
                               new UsernamePasswordAuthenticationToken(
                                       email,
                                      "GoogleAuth#1"
                               )
                       );
                       Optional<User> byUsername = userRepository.findByUsername(email);
                       Set<Role> roleSet = byUsername.get().getRoles();
                       String role = "";
                       for (Role role1: roleSet) {
                           if ("ROLE_ADMIN".equalsIgnoreCase(role1.getName().name())){
                               role = "1";
                           }else if("ROLE_USER".equalsIgnoreCase(role1.getName().name())){
                               role = "2";
                           }else {
                               role = "3";
                           }
                       }
                       SecurityContextHolder.getContext().setAuthentication(authentication);
                       String jwt = jwtProvider.generateJwtToken(authentication);
                       JwtResponse jwtResponse = new JwtResponse(jwt);
                       jwtResponse.setRole(role);;
                       return ResponseEntity.status(HttpStatus.OK).body(Optional.of(jwtResponse));

                   }
                   catch (Exception e){
                       e.printStackTrace();
                       ErrorDTO error =new ErrorDTO();
                       error.setErrorCode("401");
                       error.setMessage("Invalid Username or Password");
                       return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
                   }

               }
              else {

                   Optional<Role> userRole = roleRepository.findByName(RoleName.ROLE_USER);
                   Set<Role> roles = new HashSet<>();
                   roles.add(userRole.get());

                   String image = response.jsonPath().getString("picture");
                   String name = response.jsonPath().getString("name");


                   socialUser.setApplication("Chefs On App");
                   socialUser.setEmail(email);
                   socialUser.setName(name);
                   socialUser.setPassword(encoder.encode("GoogleAuth#1"));
                   socialUser.setRoles(roles);
                   socialUser.setUsername(email);
                   socialUser.setSurname("");
                   userRepository.save(socialUser);


                   userProfile.setEmail(email);
                   userProfile.setProfilePicture(image);
                   userProfile.setRole("user");
                   profileRepository.save(userProfile);

                   try {
                       Authentication authentication = authenticationManager.authenticate(
                               new UsernamePasswordAuthenticationToken(
                                       email,
                                       "GoogleAuth#1"
                               )
                       );
                       Optional<User> byUsername = userRepository.findByUsername(email);
                       Set<Role> roleSet = byUsername.get().getRoles();
                       String role = "";
                       for (Role role1: roleSet) {
                           if ("ROLE_ADMIN".equalsIgnoreCase(role1.getName().name())){
                               role = "1";
                           }else if("ROLE_USER".equalsIgnoreCase(role1.getName().name())){
                               role = "2";
                           }else {
                               role = "3";
                           }
                       }
                       SecurityContextHolder.getContext().setAuthentication(authentication);
                       String jwt = jwtProvider.generateJwtToken(authentication);
                       JwtResponse jwtResponse = new JwtResponse(jwt);
                       jwtResponse.setRole(role);
                       return ResponseEntity.status(HttpStatus.OK).body(Optional.of(jwtResponse));

                   }
                   catch (Exception e){
                       e.printStackTrace();
                       userRepository.delete(socialUser);
                       profileRepository.delete(userProfile);
                       ErrorDTO error =new ErrorDTO();
                       error.setErrorCode("401");
                       error.setMessage("Invalid Username or Password");
                       return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
                   }
               }
            }else{
                ErrorDTO error =new ErrorDTO();
                error.setErrorCode("401");
                error.setMessage("Google verification failed");
                logger.info("Something went wrong whilst assigning roles");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
            }

        }catch (Exception e){
            e.printStackTrace();
            ErrorDTO error =new ErrorDTO();
            error.setErrorCode("500");
            error.setMessage(e.getLocalizedMessage());
            logger.info("Something went wrong");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Optional.of(error));
        }

    }
}
