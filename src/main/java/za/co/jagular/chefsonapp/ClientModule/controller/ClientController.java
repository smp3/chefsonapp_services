package za.co.jagular.chefsonapp.ClientModule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import za.co.jagular.chefsonapp.ClientModule.dto.UpdateClientDTO;
import za.co.jagular.chefsonapp.ClientModule.services.UpdateClientProfile;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@CrossOrigin
public class ClientController {
    @Autowired
    UpdateClientProfile updateClientProfile;

    @PostMapping("/update/user/profile")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateUSerProfile(@Valid @RequestBody UpdateClientDTO updateClientDTO, @RequestHeader HashMap<String, String>headers){
        return updateClientProfile.updateClient(updateClientDTO, headers);

    }
}
