package za.co.jagular.chefsonapp.ClientModule.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.ClientModule.entities.Client;


@Component
public class ClientRepositoryImpl {
    @Autowired
    private ClientRepository clientRepository;

    public boolean saveClientInfo(Client client){
        try {
            clientRepository.save(client);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
    public boolean deleteClient(Client client){
        try {
            clientRepository.delete(client);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
    public Client findClientByUserName(String username){
        try {
          return clientRepository.findByUsernameIgnoreCase(username);

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
