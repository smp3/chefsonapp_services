package za.co.jagular.chefsonapp.ClientModule.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.co.jagular.chefsonapp.ChefModule.Utilities.AuditModel;
import za.co.jagular.chefsonapp.ChefModule.entities.Chef;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "Bookings")
public class   Booking extends AuditModel {
    @Id
    private String id;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL},
            mappedBy = "bookings")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Chef> chef = new ArrayList<>();
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL},
    mappedBy = "clientBookings")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Client> client = new ArrayList<>();

    public Booking(){
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Chef> getChef() {
        return chef;
    }

    public void setChef(List<Chef> chef) {
        this.chef = chef;
    }

    public List<Client> getClient() {
        return client;
    }

    public void setClient(List<Client> client) {
        this.client = client;
    }
}
