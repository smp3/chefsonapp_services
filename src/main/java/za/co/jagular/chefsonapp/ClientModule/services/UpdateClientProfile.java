package za.co.jagular.chefsonapp.ClientModule.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.ClientModule.dto.UpdateClientDTO;
import za.co.jagular.chefsonapp.ClientModule.entities.Client;
import za.co.jagular.chefsonapp.ClientModule.repositories.ClientRepositoryImpl;
import za.co.jagular.chefsonapp.jwt.dto.responses.ErrorDTO;
import za.co.jagular.chefsonapp.jwt.security.jwt.JwtProvider;
import za.co.jagular.chefsonapp.profile.Profile;
import za.co.jagular.chefsonapp.profile.ProfileRepository;
import za.co.jagular.chefsonapp.responses.ResponseDTO;

import java.util.HashMap;
import java.util.Optional;

@Service
public class UpdateClientProfile {
    @Autowired
    private ProfileRepository profileRepository;

    Logger logger = LoggerFactory.getLogger(UpdateClientProfile.class);
    public ResponseEntity<?> updateClient(UpdateClientDTO updateClientDTO, HashMap<String, String>headers){
        logger.info("Updating client profile");
        try {
            JwtProvider jwtProvider = new JwtProvider();
            String userEmail = jwtProvider.getUserNameFromJwtToken(headers.get("authorization").replace("Bearer ", ""));
            Optional<Profile> byEmail = profileRepository.findByEmail(userEmail);
            if (byEmail.isPresent()) {
                Profile profile = byEmail.get();
                if (updateClientDTO.getMobileNumber() != null ) {
                    profile.setMobileNumber(updateClientDTO.getMobileNumber());
                } if (updateClientDTO.getAvatar() != null ) {
                    profile.setProfilePicture(updateClientDTO.getAvatar());
                }
                    profileRepository.save(profile);
                    ResponseDTO responseDTO = new ResponseDTO();
                    responseDTO.setCode("200");
                    responseDTO.setMessage("Profile Update Success");
                    logger.info("Profile updated");
                    return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

            }else {
                ErrorDTO errorDTO = new ErrorDTO();
                errorDTO.setMessage("Not Authorized/ Invalid Token");
                errorDTO.setErrorCode("401");
                logger.error("Profile not updated, Unauthorized");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorDTO);
            }

        }catch (Exception e){
            e.printStackTrace();
            ErrorDTO errorDTO = new ErrorDTO();
            errorDTO.setMessage("Something went wrong");
            errorDTO.setErrorCode("500");
            logger.error("Something went wrong");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDTO);
        }


    }


}
