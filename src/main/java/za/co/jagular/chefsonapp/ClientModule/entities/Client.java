package za.co.jagular.chefsonapp.ClientModule.entities;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.co.jagular.chefsonapp.ChefModule.Utilities.AuditModel;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "Clients")
public class Client extends AuditModel {
    @Id
    private String id;
    @Column(columnDefinition = "TEXT")
    private String avatar;
    private String name;
    private String surname;
    private String username;
    private String contactNumber;
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "client_bookings",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "booking_id")})
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Booking> clientBookings = new ArrayList<>();

    public Client(){
        this.id = UUID.randomUUID().toString();
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Booking> getClientBookings() {
        return clientBookings;
    }

    public void setClientBookings(List<Booking> clientBookings) {
        this.clientBookings = clientBookings;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
