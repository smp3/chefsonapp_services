package za.co.jagular.chefsonapp.ClientModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ClientModule.entities.Booking;

public interface BookingRepository extends JpaRepository<Booking, String> {
}
