package za.co.jagular.chefsonapp.ClientModule.dto;

public class SocialAuthDTO {
    private String socialToken;
    private String application;

    public String getSocialToken() {
        return socialToken;
    }

    public void setSocialToken(String socialToken) {
        this.socialToken = socialToken;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }
}
