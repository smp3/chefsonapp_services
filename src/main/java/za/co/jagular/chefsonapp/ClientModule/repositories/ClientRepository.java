package za.co.jagular.chefsonapp.ClientModule.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.chefsonapp.ClientModule.entities.Client;

public interface ClientRepository extends JpaRepository<Client, String> {

    Client findByUsernameIgnoreCase(String username);
}
