package za.co.jagular.chefsonapp.ClientModule.dto;

public class UpdateClientDTO {
    private String mobileNumber;
    private String avatar;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
