package za.co.jagular.chefsonapp.otp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OTPGenerator {

    private Logger logger = LoggerFactory.getLogger(OTPGenerator.class);

    public  String generateOTP() {
        logger.info("Generating OTP");
        int randomPin   =(int)(Math.random()*9000)+1000;
        String otp  =String.valueOf(randomPin);
        logger.info("OTP Generated : "+otp);
        return otp;
    }
}
