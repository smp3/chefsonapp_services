package za.co.jagular.chefsonapp.otp;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class OTPRequestDTO {

    @NotBlank
    @Size(min=10, max = 20)
    private String mobileNumber;
    private String application;
    private String message;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
