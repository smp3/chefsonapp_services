package za.co.jagular.chefsonapp.otp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@CrossOrigin
public class OTPController {

    @Autowired
    OTPService otpService;
    @PostMapping("/generate/otp")
    public ResponseEntity<Optional<?>> generateOTP(@Valid @RequestBody OTPRequestDTO otpRequestDTO) {
        return otpService.generateOTP(otpRequestDTO);
    }
}
