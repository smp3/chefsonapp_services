package za.co.jagular.chefsonapp.otp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OTPRepository extends JpaRepository<OTP, Long> {
    Optional<OTP> findByOtpAndMobileNumber(String otp, String mobileNumber);
    Boolean existsByOtpAndMobileNumber(String otp, String mobileNumber);
}
