package za.co.jagular.chefsonapp.otp;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.chefsonapp.responses.ErrorDTO;
import za.co.jagular.chefsonapp.responses.ResponseDTO;
import za.co.jagular.chefsonapp.responses.RestLogger;

import java.security.SecureRandom;
import java.util.Optional;

import static io.restassured.RestAssured.given;

@Service
public class OTPService {
    @Autowired
    OTPRepository otpRepository;

    public ResponseEntity<Optional<?>> generateOTP(OTPRequestDTO otpRequestDTO){

        try {
            SecureRandom random = new SecureRandom();
            OTP otp = new OTP();
            otp.setMobileNumber(otpRequestDTO.getMobileNumber());
            otp.setOtp(String.valueOf(random.nextInt(9999 - 999) + 1000));
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            otp.setTimestamp(sdf.format(dt));
            otpRepository.save(otp);

            //Call sms API
            otpRequestDTO.setMessage(otp.getOtp());
            Response smsResponse = sendOTP(otpRequestDTO);
            ResponseDTO response = new ResponseDTO();
            if(smsResponse.getStatusCode() == 200){
                response.setCode("200");
                response.setMessage("OTP Sent");
                return ResponseEntity.status(HttpStatus.OK).body(Optional.of(response));
            }
            else{
                response.setCode("400");
                response.setMessage("Unable to send OTP");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Optional.of(response));
            }
        }
        catch (Exception e){
            e.printStackTrace();
            ErrorDTO error =new ErrorDTO();
            error.setErrorCode("401");
            error.setMessage("Unable To Generate OTP");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
        }
    }
    private Response sendOTP(OTPRequestDTO otpRequestDTO){

            RestAssured.baseURI = "http://localhost:8003/";
            String requestBody = RestLogger.toJSON(otpRequestDTO);
            Response response = null;
            try{
                response = given().contentType(ContentType.JSON)
                        .body(requestBody)
                        .post("/send/otp");
                System.out.println("Response :" + response.asString());
                System.out.println("Status Code :" + response.getStatusCode());

            }
            catch (Exception e){
                e.printStackTrace();

            }
            return response;

    }
}
