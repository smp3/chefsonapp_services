package za.co.jagular.chefsonapp.jwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import za.co.jagular.chefsonapp.jwt.entities.authentication.Role;
import za.co.jagular.chefsonapp.jwt.entities.authentication.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}