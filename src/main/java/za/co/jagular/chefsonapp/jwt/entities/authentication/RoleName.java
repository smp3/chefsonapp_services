package za.co.jagular.chefsonapp.jwt.entities.authentication;

public enum  RoleName {
    ROLE_USER,
    ROLE_CHEF,
    ROLE_ADMIN
}