package za.co.jagular.chefsonapp.jwt.authentication.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import za.co.jagular.chefsonapp.jwt.dto.authentication.JwtResponse;
import za.co.jagular.chefsonapp.jwt.dto.authentication.LoginForm;
import za.co.jagular.chefsonapp.jwt.dto.responses.ErrorDTO;
import za.co.jagular.chefsonapp.jwt.entities.authentication.Role;
import za.co.jagular.chefsonapp.jwt.entities.authentication.RoleName;
import za.co.jagular.chefsonapp.jwt.entities.authentication.User;
import za.co.jagular.chefsonapp.jwt.repository.RoleRepository;
import za.co.jagular.chefsonapp.jwt.repository.UserRepository;
import za.co.jagular.chefsonapp.jwt.security.jwt.JwtProvider;
import za.co.jagular.chefsonapp.jwt.dto.authentication.SignUpForm;
import za.co.jagular.chefsonapp.otp.OTP;
import za.co.jagular.chefsonapp.otp.OTPRepository;
import za.co.jagular.chefsonapp.profile.Profile;
import za.co.jagular.chefsonapp.profile.ProfileRepository;
import za.co.jagular.chefsonapp.responses.ResponseDTO;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class AuthenticationBusinessLogic {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    OTPRepository otpRepository;

    @Autowired
    ProfileRepository profileRepository;

    private Logger logger = LoggerFactory.getLogger(AuthenticationBusinessLogic.class);
    public ResponseEntity<Optional<?>>registerUser(SignUpForm signUpRequest){

        try {
            if (otpRepository.existsByOtpAndMobileNumber(signUpRequest.getOtp(), signUpRequest.getMobileNumber())) {
                if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                    ErrorDTO error = new ErrorDTO();
                    error.setErrorCode("401");
                    error.setMessage("Email Already Registered");
                    logger.error("Email already registered");
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(Optional.of(error));
                } else {

                    User user = new User();
                    user.setApplication("Chefs on app");
                    user.setEmail(signUpRequest.getEmail());
                    user.setName(signUpRequest.getName());
                    user.setSurname(signUpRequest.getSurname());
                    user.setPassword(encoder.encode(signUpRequest.getPassword()));
                    user.setUsername(signUpRequest.getEmail());
                    Set<String> strRoles = signUpRequest.getRole();
                    Set<Role> roles = new HashSet<>();

                    strRoles.forEach(role -> {
                        switch (role) {
                            case "admin":
                                Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                                roles.add(adminRole);

                                break;
                            case "chef":
                                Role pmRole = roleRepository.findByName(RoleName.ROLE_CHEF)
                                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                                roles.add(pmRole);

                                break;
                            case "user":
                                Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                                roles.add(userRole);
                                break;
                            default:
                                throw new RuntimeException("Fail! -> Cause: User Role not find.");
                        }
                    });

                    user.setRoles(roles);
                    //create default profile
                    Profile defaultProfile = new Profile();
                    defaultProfile.setEmail(signUpRequest.getEmail());
                    defaultProfile.setRole(signUpRequest.getRole().iterator().next());
                    defaultProfile.setMobileNumber(signUpRequest.getMobileNumber());
                    defaultProfile.setProfilePicture("default avatar");
                    profileRepository.save(defaultProfile);
                    logger.info("Default profile saved");
                    return ResponseEntity.status(HttpStatus.OK).body(Optional.of(userRepository.save(user)));
                }

            }else {
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setCode(HttpStatus.UNAUTHORIZED.toString());
                responseDTO.setMessage("OTP Invalid");
                logger.info("Invalid OTP");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(responseDTO));
            }


        }
        catch(RuntimeException e){
            e.printStackTrace();
            ErrorDTO error =new ErrorDTO();
            error.setErrorCode("401");
            error.setMessage(e.getLocalizedMessage());
            logger.info("Something went wrong whilst assigning roles");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
        }
        catch(Exception e){
            e.printStackTrace();
            ErrorDTO error =new ErrorDTO();
            error.setErrorCode("500");
            error.setMessage("Something went wrong, please contact administrator");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Optional.of(error));
        }
    }

    public ResponseEntity<Optional<?>> loginUser(LoginForm loginForm){
        try {

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginForm.getUsername(),
                            loginForm.getPassword()
                    )
            );
            Optional<User> byUsername = userRepository.findByUsername(loginForm.getUsername());
            Set<Role> roles = byUsername.get().getRoles();
            String role = "";
            for (Role userRole: roles) {
               if ("ROLE_ADMIN".equalsIgnoreCase(userRole.getName().name())){
                   role = "1";
               }else if("ROLE_USER".equalsIgnoreCase(userRole.getName().name())){
                   role = "2";
               }else {
                   role = "3";
               }
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generateJwtToken(authentication);
            JwtResponse response = new JwtResponse(jwt);
            response.setRole(role);

            return ResponseEntity.status(HttpStatus.OK).body(Optional.of(response));

        }
        catch (Exception e){
            e.printStackTrace();
            ErrorDTO error =new ErrorDTO();
            error.setErrorCode("401");
            error.setMessage("Invalid Username or Password");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Optional.of(error));
        }

    }
}
