package za.co.jagular.chefsonapp.jwt.authentication.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import za.co.jagular.chefsonapp.jwt.authentication.business.AuthenticationBusinessLogic;
import za.co.jagular.chefsonapp.jwt.dto.authentication.LoginForm;
import za.co.jagular.chefsonapp.jwt.dto.authentication.SignUpForm;
import za.co.jagular.chefsonapp.socialModule.SignInWithGoogle;
import za.co.jagular.chefsonapp.socialModule.SocialSignInDTO;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @Autowired
    AuthenticationBusinessLogic authenticationBusinessLogic;

    @Autowired
    SignInWithGoogle signInWithGoogle;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {
        return authenticationBusinessLogic.loginUser(loginRequest);
    }

    @PostMapping("/signup")
    public ResponseEntity<Optional<?>> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        return authenticationBusinessLogic.registerUser(signUpRequest);
    }

    @PostMapping("/continue/google")
    public ResponseEntity<?> googleSignIn(@RequestBody SocialSignInDTO socialSignInDTO) {
        return signInWithGoogle.continueWithGoogle(socialSignInDTO);
    }

}