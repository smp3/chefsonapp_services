package za.co.jagular.chefsonapp.jwt.dto.authentication;

import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class SignUpForm {
    @NotBlank
    private String name;

    @NotBlank
    private String surname;


    @NotBlank
    private String application;

    @NotBlank
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    private String password;

    private String otp;

    private String mobileNumber;

    @Column(columnDefinition = "TEXT")
    private String avatar;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSurname() {
        return surname;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<String> getRole() {
    	return this.role;
    }
    
    public void setRole(Set<String> role) {
    	this.role = role;
    }
}